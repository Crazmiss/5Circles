package at.crazmiss.fivecircles.comparators;

import java.util.Comparator;

import at.crazmiss.fivecircles.data.Venue;

public class CompareDistance implements Comparator<Venue> {

    @Override
    public int compare(Venue o1, Venue o2) {

        if(o1.getDistance() == o2.getDistance()){
            return o2.getName().compareTo(o1.getName());
        }

        return o1.getDistance() - o2.getDistance();
    }
}
