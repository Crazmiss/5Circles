package at.crazmiss.fivecircles.list;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import at.crazmiss.fivecircles.R;
import at.crazmiss.fivecircles.data.Venue;
import at.crazmiss.fivecircles.communication.Helpers;

import static android.text.Html.FROM_HTML_MODE_LEGACY;

public class VenueListAdapter extends ArrayAdapter<Venue> {

    private ArrayList<Venue> contacts = new ArrayList<>();
    private Helpers helpers = new Helpers();

    public VenueListAdapter(Context context, List<Venue> items) {
        super(context, 0, items);
        contacts.addAll(items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Venue item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_single_venue, parent, false);
        }

        if (item != null) {
            TextView tvAddress = (TextView) convertView.findViewById(R.id.textview_address);
            TextView tvName = (TextView) convertView.findViewById(R.id.textview_name);
            TextView tvDistance = (TextView) convertView.findViewById(R.id.textview_distance);

            if (tvAddress != null) {
                tvAddress.setText(item.getAddress());
            }

            if (tvName != null) {

                if (item.getUrl() != null) {
                    String URL = helpers.generateLinkedText(item);
                    Spanned htmlText;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        htmlText = Html.fromHtml(URL, Html.FROM_HTML_MODE_LEGACY);
                    } else {
                        htmlText = Html.fromHtml(URL);
                    }

                    tvName.setText(htmlText);
                } else {
                    tvName.setText(item.getName());
                }
            }

            if (tvDistance != null) {
                if (item.getDistance() != 0) {
                    tvDistance.setText(helpers.formatDistance(item.getDistance()));
                    tvDistance.setVisibility(View.VISIBLE);
                } else {
                    tvDistance.setVisibility(View.INVISIBLE);
                }
            }

        }

        return convertView;
    }

}
