package at.crazmiss.fivecircles.communication;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import at.crazmiss.fivecircles.MainActivity;
import at.crazmiss.fivecircles.data.Venue;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static at.crazmiss.fivecircles.R.string.client_id;
import static at.crazmiss.fivecircles.R.string.client_secret;
import static at.crazmiss.fivecircles.R.string.version_date;
import static at.crazmiss.fivecircles.communication.ReturnEnum.FAILED;
import static at.crazmiss.fivecircles.communication.ReturnEnum.NETWORK_ERROR;
import static at.crazmiss.fivecircles.communication.ReturnEnum.SUCCESS;

public class VenueRequests {

    private Context venueContext;
    private Helpers helpers = new Helpers();
    private static ArrayList<Venue> newVenues = new ArrayList<>();

    public VenueRequests(Context context) {
        venueContext = context;
    }


    public void sendVenuesRequest(final String ll, final String distance) {
        newVenues.clear();

        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

            String requestResponse;

            private OnRequestCompleted taskCompleted = (OnRequestCompleted) venueContext;

            @Override
            protected void onPostExecute(String s) {
                taskCompleted.onTaskCompleted(s);
            }

            @Override
            protected String doInBackground(Void... params) {

                if (!helpers.isNetworkAvailable(venueContext)) {
                    requestResponse = NETWORK_ERROR.toString();
                }

                if (ll == null) {
                    requestResponse = FAILED.toString();
                }

                try {

                    HttpUrl url = new HttpUrl.Builder()
                            .scheme("https")
                            .host("api.foursquare.com")
                            .addPathSegment("v2")
                            .addPathSegment("venues")
                            .addPathSegment("search")
                            .addQueryParameter("ll", ll)
                            .addQueryParameter("radius", distance)
                            .addQueryParameter("intent", "browse")
                            .addQueryParameter("client_id", venueContext.getString(client_id))
                            .addQueryParameter("client_secret", venueContext.getString(client_secret))
                            .addQueryParameter("v", venueContext.getString(version_date))
                            .build();

                    OkHttpClient client = new OkHttpClient.Builder()
                            .build();

                    Request request = new Request.Builder()
                            .url(url)
                            .get()
                            .build();

                    Response response = client.newCall(request).execute();
                    String result = response.body().string();

                    if (result == null) {
                        requestResponse = FAILED.toString();
                    } else {
                        newVenues.addAll(helpers.readVenues(result));

                        if (newVenues != null) {
                            requestResponse = SUCCESS.toString();
                        } else {
                            requestResponse = FAILED.toString();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    requestResponse = FAILED.toString();
                }
                return requestResponse;
            }
        }.execute();


        try {
            task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    public void sendVenuesRequestCity(final String near, final String distance) {
        newVenues.clear();

        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

            String requestResponse;

            private OnRequestCompleted taskCompleted = (OnRequestCompleted) venueContext;

            @Override
            protected void onPostExecute(String s) {
                taskCompleted.onTaskCompleted(s);
            }

            @Override
            protected String doInBackground(Void... params) {

                if (!helpers.isNetworkAvailable(venueContext)) {
                    requestResponse = NETWORK_ERROR.toString();
                }

                if (near == null) {
                    requestResponse = FAILED.toString();
                }

                try {

                    HttpUrl url = new HttpUrl.Builder()
                            .scheme("https")
                            .host("api.foursquare.com")
                            .addPathSegment("v2")
                            .addPathSegment("venues")
                            .addPathSegment("search")
                            .addQueryParameter("near", near)
                            .addQueryParameter("radius", distance)
                            .addQueryParameter("intent", "browse")
                            .addQueryParameter("client_id", venueContext.getString(client_id))
                            .addQueryParameter("client_secret", venueContext.getString(client_secret))
                            .addQueryParameter("v", venueContext.getString(version_date))
                            .build();

                    OkHttpClient client = new OkHttpClient.Builder()
                            .build();

                    Request request = new Request.Builder()
                            .url(url)
                            .get()
                            .build();

                    Response response = client.newCall(request).execute();
                    String result = response.body().string();

                    if (result == null) {
                        requestResponse = FAILED.toString();
                    } else {
                        newVenues.addAll(helpers.readVenues(result));

                        if (newVenues != null) {
                            requestResponse = SUCCESS.toString();
                        } else {
                            requestResponse = FAILED.toString();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    requestResponse = FAILED.toString();
                }
                return requestResponse;
            }
        }.execute();


        try {
            task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }


    public ArrayList<Venue> getNewVenues() {
        return newVenues;
    }
}
