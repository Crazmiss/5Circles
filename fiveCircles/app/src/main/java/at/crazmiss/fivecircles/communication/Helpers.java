package at.crazmiss.fivecircles.communication;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import at.crazmiss.fivecircles.MainActivity;
import at.crazmiss.fivecircles.data.Venue;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.Context.LOCATION_SERVICE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.location.LocationManager.NETWORK_PROVIDER;

public class Helpers {
    private static final int LOCATION_PERMISSION = 1;

    public String generateLinkedText(Venue venue) {
          String text = "<a href='" + venue.getUrl() + "'>" + venue.getName() + "</a>";
        return text;
    }


    public boolean isNetworkAvailable(Context venueContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) venueContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void printVenues(ArrayList<Venue> venues) {

        for (int i = 0; i < venues.size(); i++) {
            Log.e("venue", "numbers: " + venues.size() + " " + i + " " + venues.get(i).toString());
        }

    }

    public void getVenuesByLocation(final Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                VenueRequests venueRequests = new VenueRequests(context);
                venueRequests.sendVenuesRequest(location.getLatitude() + "," + location.getLongitude(), MainActivity.getDISTANCE());
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        if (ActivityCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity) context, new String[]{ACCESS_FINE_LOCATION,ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION);
            return;
        }

        locationManager.requestSingleUpdate(NETWORK_PROVIDER, locationListener, null);
    }

    public String formatDistance(int distance) {
        String formattedDistance;

        if (distance > 999) {
            float distanceChange = (float) distance / 1000.f;

            formattedDistance = String.format("%.2f km", distanceChange);

        } else {
            formattedDistance = distance + " m";
        }

        return formattedDistance;

    }


    public ArrayList<Venue> readVenues(String result) {
        ArrayList<Venue> newVenues = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(result);

            JSONObject obj = null;
            if (object.has("response")) {
                obj = object.getJSONObject("response");
            }

            JSONArray array = null;
            if (obj.has("venues")) {
                array = obj.getJSONArray("venues");
            }

            for (int i = 0; i < array.length(); i++) {

                JSONObject info = array.getJSONObject(i);
                Venue v = new Venue();

                if (info.has("name")) {
                    v.setName(info.getString("name"));
                }

                if (info.has("location") && info.getJSONObject("location").has("formattedAddress")) {
                    JSONArray formattedAddress = info.getJSONObject("location").getJSONArray("formattedAddress");
                    String address = "";

                    for (int j = 0; j < formattedAddress.length(); j++) {
                        address += formattedAddress.get(j);
                        if (j != formattedAddress.length()) {
                            address += "\n";
                        }
                    }

                    v.setAddress(address);
                }

                if (info.has("location") && info.getJSONObject("location").has("distance")) {
                    v.setDistance(info.getJSONObject("location").getInt("distance"));
                }

                if (info.has("url")) {
                    v.setUrl(info.getString("url"));
                }

                newVenues.add(v);
            }

            Log.e("venues", array.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return newVenues;
    }


}
