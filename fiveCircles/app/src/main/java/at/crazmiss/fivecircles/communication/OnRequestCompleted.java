package at.crazmiss.fivecircles.communication;

public interface OnRequestCompleted {
    void onTaskCompleted(String response);
}