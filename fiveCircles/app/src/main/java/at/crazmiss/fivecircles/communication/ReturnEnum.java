package at.crazmiss.fivecircles.communication;

public enum ReturnEnum {

    FAILED,
    SUCCESS,
    NETWORK_ERROR
}
