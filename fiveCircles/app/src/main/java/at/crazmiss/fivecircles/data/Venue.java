package at.crazmiss.fivecircles.data;

public class Venue {

    private String name;
    private String address;
    private String url;
    private int distance;

    public Venue(String name, String address, String url, int distance) {
        this.name = name;
        this.address = address;
        this.url = url;
        this.distance = distance;
    }

    public Venue(String name, String address, int distance) {
        this(name, address, null, distance);
    }


    public Venue() {
        this(null, null, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Venue{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", url='" + url + '\'' +
                ", categories=" + distance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Venue venue = (Venue) o;

        if (distance != venue.distance) return false;
        return name != null ? name.equals(venue.name) : venue.name == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + distance;
        return result;
    }
}
