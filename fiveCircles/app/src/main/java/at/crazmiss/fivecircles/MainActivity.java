package at.crazmiss.fivecircles;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import at.crazmiss.fivecircles.communication.Helpers;
import at.crazmiss.fivecircles.communication.OnRequestCompleted;
import at.crazmiss.fivecircles.communication.ReturnEnum;
import at.crazmiss.fivecircles.communication.VenueRequests;
import at.crazmiss.fivecircles.comparators.CompareDistance;
import at.crazmiss.fivecircles.data.Venue;
import at.crazmiss.fivecircles.list.VenueListAdapter;

public class MainActivity extends AppCompatActivity implements OnRequestCompleted {

    private static final int LOCATION_PERMISSION = 1;
    private static String DISTANCE = "5000";
    private ArrayList<Venue> venues = new ArrayList<>();
    private VenueListAdapter adapter;
    private VenueRequests venueRequests = new VenueRequests(this);
    private Helpers helpers = new Helpers();
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        helpers.getVenuesByLocation(context);

        adapter = new VenueListAdapter(this.getApplicationContext(), venues);
        ListView list = (ListView) this.findViewById(R.id.listview_venues);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (venues.get(position).getUrl() != null) {
                    Uri uri = Uri.parse(venues.get(position).getUrl());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setData(uri);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public void onTaskCompleted(String response) {
        switch (ReturnEnum.valueOf(response)) {

            case SUCCESS:
                venues.clear();
                venues.addAll(venueRequests.getNewVenues());
                //     helpers.printVenues(venueRequests.getNewVenues());
                Collections.sort(venues, new CompareDistance());
                adapter.notifyDataSetChanged();
                Toast.makeText(context, "Done fetching Venues", Toast.LENGTH_SHORT).show();
                break;

            case FAILED:
                Toast.makeText(context, "Something Failed", Toast.LENGTH_SHORT).show();
                break;

            case NETWORK_ERROR:
                Toast.makeText(context, "There was a network error", Toast.LENGTH_SHORT).show();
                break;

            default:
                Toast.makeText(context, "UNKNOWN", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case LOCATION_PERMISSION:

                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    helpers.getVenuesByLocation(context);
                    findViewById(R.id.textview_location_available).setVisibility(View.GONE);
                    findViewById(R.id.listview_venues).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.listview_venues).setVisibility(View.GONE);
                    findViewById(R.id.textview_location_available).setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {

            case R.id.position:
                helpers.getVenuesByLocation(context);
                break;

            case R.id.search:
                MenuItemCompat.setActionView(item, R.layout.item_location_search);

                EditText editText = (EditText) item.getActionView().findViewById(R.id.actionbar_notification_editview);
                editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        boolean handled = false;
                        if (actionId == EditorInfo.IME_ACTION_SEND) {
                            venueRequests.sendVenuesRequestCity(v.getText().toString(), DISTANCE);
                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            handled = true;
                        }
                        return handled;
                    }
                });

                break;

            case R.id.distance:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = this.getLayoutInflater();
                View popupView = inflater.inflate(R.layout.layout_distance_popup, null);

                final SeekBar seekBar = (SeekBar) popupView.findViewById(R.id.seekbar);
                final TextView textView = (TextView) popupView.findViewById(R.id.textview_distance_progress);

                seekBar.setProgress(Integer.parseInt(DISTANCE));
                textView.setText(helpers.formatDistance(Integer.parseInt(DISTANCE)));

                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        textView.setText(helpers.formatDistance(progress));
                        DISTANCE = String.valueOf(progress);
                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });

                builder.setView(popupView);
                builder.setTitle("Distance");

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        helpers.getVenuesByLocation(context);
                    }
                });


                builder.show();
                break;

            default:
                Toast.makeText(this, "None selected", Toast.LENGTH_SHORT).show();
                break;
        }

        return true;
    }


    public void onElementSearch(View view) {
        View parentView = (View) view.getParent();
        EditText tv = (EditText) parentView.findViewById(R.id.actionbar_notification_editview);
        venueRequests.sendVenuesRequestCity(tv.getText().toString(), DISTANCE);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        findViewById(R.id.textview_location_available).setVisibility(View.GONE);
        findViewById(R.id.listview_venues).setVisibility(View.VISIBLE);

    }

    public static String getDISTANCE() {
        return DISTANCE;
    }

}

